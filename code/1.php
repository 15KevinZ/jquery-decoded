<span class="com" hover="Welcome to jQuery Decoded! Hover over sections of code (or even comments) to discover how it works.">/*!
 * jQuery JavaScript Library v1.11.1
 * http://jquery.com/
 *
 * Includes <span hover="Sizzle.js is a CSS selector engine written in pure Javascript.">Sizzle.js</span>
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
 * Released under <span hover="The MIT license allows you to use this code for both private and commercial applications, as long as you include this copyright notice.">the MIT license</span>
 * http://jquery.org/license
 *
 * Date: 2014-05-01T17:42Z
 */</span><span class="pln">

</span></span><span class="pun">(</span><span hover="This anonymous function is executed when the javascript is loaded. It determines an appropriate window object (global) and passes it to the factory method."><span class="kwd">function</span><span class="pun">(</span><span class="pln"> </span><span class="kwd" hover="In normal browsers, the globals parameter is the current window object.">global</span><span class="pun">,</span><span class="pln" hover="Factory is the callback function which will be called once an appropriate global (window) is set."> factory </span><span class="pun">)</span><span class="pln"> </span><span class="pun">{</span><span class="pln">
  </span><span hover="This block of code is executed when jQuery is used in a window-less environment such as Node."><span class="kwd">if</span><span class="pln"> </span><span class="pun">(</span><span class="pln"> </span><span class="kwd">typeof</span><span class="pln"> </span><span class="kwd">module</span><span class="pln"> </span><span class="pun">===</span><span class="pln"> </span><span class="str">"object"</span><span class="pln"> </span><span class="pun">&amp;&amp;</span><span class="pln"> </span><span class="kwd">typeof</span><span class="pln"> </span><span class="kwd">module</span><span class="pun">.</span><span class="pln">exports </span><span class="pun">===</span><span class="pln"> </span><span class="str">"object"</span><span class="pln"> </span><span class="pun">)</span><span class="pln"> </span><span class="pun">{</span><span class="pln">
    </span><span class="com">// For <span hover="CommonJS refers to javascript that is executed outside of the browser. Examples include server-side javascript and javascript-based mobile apps.">CommonJS and CommonJS-like environments</span> where a proper window is present,</span><span class="pln">
    </span><span class="com">// execute the factory and get jQuery</span><span class="pln">
    </span><span class="com">// For environments that do not inherently posses a window with a document</span><span class="pln">
    </span><span class="com">// (such as <span hover="A back-end javascript framework which is commonly used to power scalable web applications.">Node.js</span>), expose a jQuery-making factory as module.exports</span><span class="pln">
    </span><span class="com">// This accentuates the need for the creation of a real window</span><span class="pln">
    </span><span class="com">// e.g. var jQuery = require("jquery")(window);</span><span class="pln">
    </span><span class="com">// See ticket #14549 for more info</span><span class="pln">
    </span><span class="kwd">module</span><span class="pun">.</span><span class="pln">exports </span><span class="pun">=</span><span class="pln"> </span><span class="kwd">global</span><span class="pun">.</span><span class="pln">document </span><span class="pun">?</span><span class="pln">
      factory</span><span class="pun">(</span><span class="pln"> </span><span class="kwd">global</span><span class="pun">,</span><span class="pln"> </span><span class="kwd">true</span><span class="pln"> </span><span class="pun">)</span><span class="pln"> </span><span class="pun">:</span><span class="pln">
      </span><span class="kwd">function</span><span class="pun">(</span><span class="pln"> w </span><span class="pun">)</span><span class="pln"> </span><span class="pun">{</span><span class="pln">
        </span><span class="kwd">if</span><span class="pln"> </span><span class="pun">(</span><span class="pln"> </span><span class="pun">!</span><span class="pln">w</span><span class="pun">.</span><span class="pln">document </span><span class="pun">)</span><span class="pln"> </span><span class="pun">{</span><span class="pln">
          </span><span class="kwd">throw</span><span class="pln"> </span><span class="kwd">new</span><span class="pln"> </span><span class="typ">Error</span><span class="pun">(</span><span class="pln"> </span><span class="str">"jQuery requires a window with a document"</span><span class="pln"> </span><span class="pun">);</span><span class="pln">
        </span><span class="pun">}</span><span class="pln">
        </span><span class="kwd">return</span><span class="pln"> factory</span><span class="pun">(</span><span class="pln"> w </span><span class="pun">);</span><span class="pln">
      </span><span class="pun">};</span><span class="pln">
  </span><span class="pun">}</span></span><span class="pln"> </span><span hover="This block of code should be executed in normal browsers."><span class="kwd">else</span><span class="pln"> </span><span class="pun">{</span><span class="pln">
    factory</span><span class="pun">(</span><span class="pln"> </span><span class="kwd">global</span><span class="pln"> </span><span class="pun">);</span><span class="pln">
  </span><span class="pun">}</span></span><span class="pln">
</span><span class="com">// Pass this if window is not defined yet</span><span class="pln">
</span><span class="pun">}</span></span>